## COMPRESS VIDÉO

Some commands : 

ffmpeg -n -loglevel error -i arke_tubes.mp4 -an -vcodec libx264 -vf "scale=iw/4:ih/4" -crf 28 -preset faster -tune film outputfilename.mp4

ffmpeg -i video.mp4 -vcodec h264 -b:v 1000k -acodec mp3 output.mp4

ffmpeg -i source.mp4 -c:v libvpx-vp9 -b:v 0.33M -c:a libopus -b:a 96k -filter:v scale=960x540 target.webm

ffmpeg -i input.mp4 -c:v libx265 -preset veryfast -tag:v hvc1 -vf format=yuv420p -c:a copy output.mp4

Here is a 2 pass example. Pretty hard coded but it really puts on the squeeze :

#!/bin/bash
ffmpeg -y -i "$1"  -c:v libvpx-vp9 -pass 1 -deadline best -crf 30 -b:v 664k -c:a libopus -f webm /dev/null && ffmpeg -i "$1"  -c:v libvpx-vp9 -pass 2  -crf 30 -b:v 664k  -c:a libopus -strict -2 "$2
